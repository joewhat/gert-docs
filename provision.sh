#!/usr/bin/env bash

apt-get update
apt-get -y upgrade
apt-get install -y git
apt-get install -y htop
apt-get install -y g++
apt-get install -y build-essential
apt-get install -y zip

# hostname
hostname gertdocs
echo "127.0.0.1 gertdocs" >> /etc/hosts
echo "gertdocs" > /etc/hostname

# .bashrc
echo "cd /vagrant/gertdocs" >> /home/ubuntu/.bashrc

# pip
# apt-get install -y python2.7-dev
apt-get install -y python-pip
pip install --upgrade pip

# mkdocs
pip install mkdocs
